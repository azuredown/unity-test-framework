# Unity Test Framework

A framework that allows for easy testing from within the game.

## Getting Started

This framework relies on `Tester.initializeTests()` being called to run tests. The recommended way to do that is to use the IngameDebugConsole. It has its own README which explains how to use it. Alternately you can use your own custom solution to call `initializeTests()`. If you do this be sure to remove all references of the `IngameDebugConsole` from `Tester`.

Next `Tester.update()` must be called every frame. An easy way to do this is have some other `MonoBehaviour` call it from its own `Update`. Alternately you can just convert `Tester` into a `MonoBehaviour` and convert `update()` to `Update()`.

Next you will need to write your tests. A test is just a coroutine (an `IEnumerator` object) that throws an exception upon reaching an error.

Next enqueue all your tests in `Tester.initializeTests()` by hard coding them before the line `totalTests = tests.Count`. Each line should look something like this.

`tests.Enqueue(TestFAB.TestFABClicking(counter));`

Finally to run your tests run `initializeTests()` (if you're using IngameDebugConsole this is done by typing in the command 'RunTests') The results of the test will be written to the console.